<?php

/**
 * Payment method form base block
 */
class IGN_Payment_Block_Form extends Mage_Payment_Block_Form
{
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('ignpayment/form.phtml');
    }
}
